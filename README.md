# Encryption for Friends and Gamers

Due to the current political climate which has brought us 'encrypted message access laws' there is at times a need for additional security when sending private messages to associates.
To help make it easy to add a [One-time pad](https://en.wikipedia.org/wiki/One-time_pad) to your most important messages, e4fag is at your disposal. 

Simply generate a large number of secure keys and give them to your friends in the real world (never share the keys with anyone else or over the internet) and whenever the two of you (Or your gaming group) need to send a small message super securely encrypt your message using one of the numbered keys.
You may then share the encrypted text and the key number used to allow them to decrypt your message!

This should be pretty safe and secure as long as you generate strong keys and have no more than 512 characters per message.
To ensure the highest level of security generate many keys and only ever use each key once.
Never transfer keys over the internet, always do it in person to ensure that they remain perfectly secret.

## How to generate keys.
To generate keys simply run generateKeys.html and click "Generate Keys".
Once generated they will be automatically downloaded by your brower for you to give to your friends!

![Example Key Generation](generateKeys.png)

##Requirements
To make the UI look better we use [Bootstrap](https://getbootstrap.com/docs/4.1/getting-started/download/) in this project.
If you want the UI to look smooth and correct you will need to download the bootstrap 4 files into a directory called 'bootstrap-4'.

##Supported Clients
Currently PHP7+, Chrome and Edge browsers are supported. (Edge is very slow.)
It may work to some extent on other versions or browsers however these are untested.

## Example Encryption
![Example Encryption](encryptDemo.png)

## Example Decryption
![Example Decryption](decryptDemo.png)


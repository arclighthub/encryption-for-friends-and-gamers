<?php
date_default_timezone_set('Australia/Sydney');
$filename = "keys ".date('Y-m-d.H_i_s').".txt";
//Generate 1000 keys per batch, ~84 keys per day.
for($x = 0; $x < 1000; $x++){
    $key = "\nKey ".$x.":\n";
    $data = '';
    $keyset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"; //You may choose to add more characters
    $keysetSize = strlen($keyset);
	//Generate one key every ~17 minutes (one byte per second) to ensure maximum entropy.
    for($y = 0; $y < 1024; $y++){
		$data =  random_int(0,$keysetSize-1); //PHP > 7
        $key .= $keyset[$data]; //PHP > 7
		sleep(1);
    }
    $key.="\n";
	echo $key;
    file_put_contents($filename, $key, FILE_APPEND);
}

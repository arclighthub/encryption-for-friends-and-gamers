<?php

//Constants
$codeKey = "";

//Main loop
const POLY_KEY_1 = 7;
const POLY_KEY_2 = 11;
const POLY_KEY_3 = 19;
const POLY_KEY_4 = 23;
//Initial Loop
const POLY_KEY_5 = 11;
const POLY_KEY_6 = 19;
const POLY_KEY_7 = 47;

//Iterations for loop
const ITERATIONS_1024 = 641;
const ITERATIONS_4096 = 3002411;

const VECTOR_MOD = 500000681;

//Code keywords for IV. Need at least 8.
$keyWords = [
    'duck',
    'alpha',
    'niner',
    'tests',
    'samba',
    'phone',
    'doe',
    'shoe'
];

/**
 * Usages:
 *
 * Encrypt:
 * $encodedText = Ecrypt("The quick brown fox jumps over the lazy dog!", $keyConst);
 *
 * Decrypt:
 * Dcrypt($encodedText, $keyConst);
 *
 * Run a test:
 * testRun($keyConst);
 * 
 * Remember to only use this function with the provided OTP. Other usages are highly discouraged.
 */

echo "What do you want to do? Enter 1 for encrypt and 2 for decrypt.\n";
$input = rtrim(fgets(STDIN));

if($input == 1){
	//Encrypt
	echo "Please enter the secret key you wish to use.\n";
	$codeKey = rtrim(fgets(STDIN));
	//Verify Key size.
	if(strlen($codeKey) < 128){
		echo "Not enough entropy to meaningfully encrypt.\n";
		die();
	} else if(strlen($codeKey) < 1024){
		//If the code key is not long enough integrate it with the hash.
		foreach ($keyWords as $keyWord){
			$keyConst .= hash('whirlpool', $keyWord.$codeKey); //Chosen IV, minimum 1024 byte.
		}
	} else {
		$keyConst = $codeKey; //Ideally this should happen rather than any of the above.
	}
	echo "Please enter the secret message to encrypt.\n";
	$secretData = rtrim(fgets(STDIN));
	if(strlen($secretData) > 1024){
		echo "Error, you may only encrypt a maximum of 1024 bytes at a time.\n";
		die();
	} else if(strlen($secretData) > 512){
		echo "The given data is too long to be perfectly encrypted. Are you sure you want to continue? (y/n)\n";
		$prompt = rtrim(fgets(STDIN));
		if($prompt != "y"){
			echo "Encryption Cancelled\n";
			die();
		}
	}
	$encodedText = Ecrypt($secretData, $keyConst);
	echo "\nData encrypted:\n";
	echo $encodedText;
	echo "\n\n";
} else if($input == 2){
	//Decrypt
	echo "Please enter the secret key which was used.\n";
	$codeKey = rtrim(fgets(STDIN));
	echo "Please enter the encrypted data.\n";
	$encodedText = rtrim(fgets(STDIN));
	Dcrypt($encodedText, $codeKey);
	echo "\n\n";
} else {
	echo "You did not enter a valid option.\n";
	die();
}

/**
 * Run a test using a test text.
 *
 * @param $keyConst
 */
function testRun($keyConst){
    $encodedText = Ecrypt("The quick brown fox jumps over the lazy dog!", $keyConst);
    echo "\nEncoded Data: ";
    echo "\n".$encodedText."\n";
    Dcrypt($encodedText, $keyConst);
    Dcrypt("vckHw2HneT6pyqowLgQO1PvcdF/aZwP/XDvxKykGJc0pzKPmPqmlGEsIMFPIlk9hKr6wdvA4ggQ3dftRZtG4EFIu/xSE1iU/KT2EZpA1amShGRqssUdJWcm3t1y0MFtlQMBW0LDNA4Mi/Ru13ocgLbgGYNsuFyJQTrdBcsFxtEntxvH9diOmKW6oRg9HBxLHGPzx0S1yp1LKaxX72qpdb+ZYODmFCLpv4+vjJhEleGv6NR5KxIFI//zQtCt1AeCB5gKRO1HmHzu/MlQgFbewFyU2snh84A2hOKtkuRkQpIsDPruNEGBw63ypDzhWEkAFYIBBNiZAanJlSbVQipng3Z82RUt8V0vq4DgtHnbxgW2+IBtgaEGdmItpqneoRWWMmtg5/8m54pCK4gtHeCzqpFV4T7m8YJcb5xM+KFpEugJVAGTGFHLp8ej/Y1FcYfm7L3Fz8N4lXqDsMUr9XHLlM14Hlglk+PLgiEPozt6yVmyH0RXnFs/+b6iwC0Fg4DA+Nsoo8bf+W4T5I912KR3HECXhy3KKmQlERcciDXhPaKlotR/2ujZCtDd/37JI/HPZYlATxjZcpZRH6FrCfRcxj6ZrBAIywCqqIfQvMDBiTdXLSIL5T/U3gbW2FqMotAo/Of4T8n42ZFHroo+NUII96roj46wFN5AGC6NXvVhu+aDkVO0JnEVwkTEbcMlIF5eP6pG8NTsrD5BZZDQME3ToMrEJsYibPY9vajH2iGSR76zh7P76l/LwYmqvtgGXGXw52zcBO14C1sRp4R4nSI2fO5tqk2LX/z9Znm5N8QYa879LwFzCr73t/rU7VRIFOjEzLQMqhbf8HUXT5zh1ILkwJwozDvCBBCPvo34m58oZAgdccCFcUsZ2E/GR90d9KIJaqIRVCnDgjWwzdmHGQ4NpT3wWL0UTuQSv9bxVGRvbm66ZlApHgKeMx68UX2c0GPxWhUR5XRlRln8J4tJ1risyHRalM/lq4Vu9PFMzNH3YhcR4Nw/dCRbfH8J2O0wZDbxohErZM3zyNFyTcllaxyMxCMIjRMmsc+MtZ/oAbUkoS6DPk4phAH93+58XcP4+tXH6XWhkq78836shZVPds2K0YgMGbgF230QsPZdwxW/x7CNCr1G4UJhoJlWdMpQWvkKORzoBAKqfPfkBcB5QTVkqjJshArK/4OYOm80SfUKvcyd45oM1P4pquD7HFWiZtTUmb7ND+e07FtwSwAcHqsBn0zDltZSeFz5cxoEyPJSYMzm+je/3wNc93y+HENQvVHItvyg2zmauRI0zYWwfpGgXoDfWB2kQLDxQs6XjErgBgoR+cRri0/9nDLd0JA3JFTRbs9yEacJ6NlLzCx2e4kyfxg==", $keyConst);
}

/**
 * Decrypt a given text using the given cipher
 *
 * @param $bitwiseKey
 * @param $cyphertext
 */
function Dcrypt($cyphertext, $bitwiseKey){
    $masked = base64_decode($cyphertext);
	if(strlen($masked) > 1024){
		echo "Error, invalid data.\n";
		die();
	}
    $undone = rot8192Poly($bitwiseKey ^ $masked, $bitwiseKey,true);
    $undone = explode('\0', $undone)[0];
    echo "\nDecrypted Data: " . $undone."\n";
}

/**
 * Encrypt a given text using the given cipher
 *
 * @param $plainText
 * @param $bitwiseKey
 * @return string
 */
function Ecrypt($plainText, $bitwiseKey){
    $plainText = rot8192Poly($plainText, $bitwiseKey, false, true);
    $masked = $bitwiseKey ^ $plainText;
    $encoded = base64_encode($masked);
    return $encoded;
}


/**
 * Convert string to binary stream
 *
 * @param $value
 * @return string
 */
function convertToBin($value){
    $data = "";
    $initialLength = strlen($value);
    for($x = 0; $x < $initialLength; $x++){
        $bin = decbin(ord($value[$x]));
        for($y = 0; strlen($bin) < 8; $y++){
            $bin = "0" . $bin; //Pad the stream correctly.
        }
        $data .= $bin;
    }
    return $data;
}

/**
 * Convert binary stream to string
 *
 * @param $value
 * @return string
 */
function convertToChar($value){
    $data = '';
    $initialLength = strlen($value);
    for($x = 0; $x < $initialLength; $x+=8){
        $pack = substr($value,$x,8);
        $data .= chr(bindec($pack));
    }
    return $data;
}

/**
 * Generate the offset for the power mod.
 *
 * @param $key
 * @return int|string
 */
function generateVectorOffset($key){
    $offsetLength = strlen($key);
    $vectorOffset = 0;
    for($x = 0; $x < $offsetLength; $x++){
        $vectorOffset .= hexdec($key[$x]);
        $vectorOffset %= VECTOR_MOD;
    }
    return $vectorOffset;
}

/**
 * Rotate up to 512 bytes safely.. more are possible.
 */
function rot1024Poly($plainText, $key, $reverse){
    $length = strlen($plainText);
    $iterations = ITERATIONS_1024; //The Magic number.
    $magicOffset = 191+generateVectorOffset($key);
    for($x = 0; $x < $iterations; $x++){
        $y = ($reverse ? ($iterations-1)-$x : $x)%$length;
        $location = (POLY_KEY_5*pow($y,4)+POLY_KEY_6*pow($y,3)+POLY_KEY_7*pow($y,2)+$magicOffset)%$length; //Semi-prime location.
        $temp = $plainText[$y];
        $plainText[$y] = $plainText[$location];
        $plainText[$location] = $temp;
    }
    return $plainText;
}

/**
 * KDF
 * Take up to 1024 byte string and spread out bits.
 */
function rot8192Poly($plainText, $key, $reverse = false,  $fill = false) {
    if($fill){
        //Ensure key length is long enough.
        for($x = 0; strlen($plainText) < 1024; $x++){
            if($x == 0){
                $plainText .= '\0'; //#padding
            } else {
                $plainText .= $key[$x];
            }
        } 
    }

    //Initial rotations.. Like an onion.
    if(!$reverse) {
        $plainText = rot1024Poly($plainText, $key, $reverse);
    }

    //convert to binary data.
    $binaryText = convertToBin($plainText);
    $length = strlen($binaryText);
    $iterations = ITERATIONS_4096; //The Magic number.
    $magicOffset = (1471+generateVectorOffset($key))%$length;
    for($x = 0; $x < $iterations; $x++){
        $y = ($reverse ? ($iterations-1)-$x : $x)%$length;
        $location = (POLY_KEY_1*pow($y,3.3)+POLY_KEY_2*pow($y,3.1)+POLY_KEY_3*pow($y,3)+POLY_KEY_4*pow($y,2)+$magicOffset)%$length; //Semi-prime location.
        $temp = $binaryText[$y];
        $binaryText[$y] = $binaryText[$location];
        $binaryText[$location] = $temp;
        //echo "\nMoving from ".$y." to ".$location."\n";
    }

    if($reverse) {
        $binaryText = convertToChar($binaryText);
        return rot1024Poly($binaryText, $key, $reverse);
    } else {
        return convertToChar($binaryText);
    }
}
